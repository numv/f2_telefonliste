import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';


@Injectable()
export class MService {
  socketObserver: any;
  socketService: any;

  ws: any;
  socketHost: string = '';

  constructor(public storage: Storage) {

    this.socketService = Observable.create(observer => {
      this.socketObserver = observer;
    });

  }


  initialize() {
    this.storage.get('server').then((value) => {
      if (value) {
        this.socketHost = value;
        console.log("Connecting..");
        this.connect();
        
      }
      else {
        this.socketHost = '';
      }
    }).catch(() => this.socketHost = '');
  }

  connect() {
    if (this.socketHost == '') {
      if (this.socketObserver != null)
        this.socketObserver.next({ category: 'disconnect', message: 'user disconnect' });
      else
        console.log("no server");
    }
    else {


      if ("WebSocket" in window) {
        this.ws = new WebSocket(this.socketHost);
        this.ws.caller = this;

        this.ws.onopen = function () {
          // console.log("Service connected.");
          this.caller.socketObserver.next({ category: 'connect', message: 'user connected' });
        };
        this.ws.onmessage = function (evt) {
          // console.log("WS<msg>: ", evt);
          this.caller.socketObserver.next({ category: 'onMessage', message: evt });
        };

        this.ws.onclose = function () {
          // console.log("Service disconnected.");
          this.caller.socketObserver.next({ category: 'disconnect', message: 'user disconnect' });
        };
      }
    }
  }

  sendMessage(message) {
    // console.log('in sendMessage and socket is: ', this.ws);
    this.ws.send(message);
    //this.socket.emit('message', message);
    this.socketObserver.next({ category: 'sendMessage', message: message });

  }

}
