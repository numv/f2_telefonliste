import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';

import { MService } from '../providers/mservice/mservice';

import { AndroidFullScreen } from '@ionic-native/android-full-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public socket: MService, public storage: Storage, private androidFullScreen: AndroidFullScreen) {


    this.rootPage = HomePage;
    this.storage.get('server').then((value) => {
      if (value) {
        this.rootPage = HomePage;
      }
      else {
        this.rootPage = TabsPage;
      }
    }).catch(() => this.rootPage = TabsPage);
    

    

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.androidFullScreen.isImmersiveModeSupported()
        .then(() => this.androidFullScreen.immersiveMode())
        .catch((error: any) => console.log(error));
    });

    this.initializeApp();
  }



  initializeApp() {
    this.socket.initialize();

    this.socket.socketService.subscribe(event => {
      console.log('ws: ', event);
    });
  }
}

