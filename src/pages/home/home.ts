import { Component, NgZone } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { MService } from '../../providers/mservice/mservice';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  entries = new Array();
  zone: NgZone;
  connected: boolean = false;
  activeprompt = null;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public socket: MService, public storage: Storage) {

    this.zone = new NgZone({ enableLongStackTrace: false });
    this.socket.socketService.subscribe(event => {
      console.log('ws-msg: ', event);
      if (event.category === 'onMessage') {

        this.zone.run(() => {
          
          var data = event.message.data;

          if (data.startsWith("[")) {

            let obj = JSON.parse(data);

            this.entries.splice(0, this.entries.length);
            obj.forEach((n) => {
              this.entries.push(n);
            });
          }
          /*
          else if (data.startsWith('Dialing: '))
          {
            data = data.substr('Dialing: '.length);

            let prompt = this.alertCtrl.create({
              
              title: 'Anruf läuft (' + data +')',
              buttons: [
                {
                  text: 'Abbrechen',
                  handler: data => {
                  }
                },
                {
                  text: 'Auflegen',
                  handler: data => {
                    this.wsSend("AUFLEGEN");
                  }
                }
              ]
            });
            prompt.present();

            if (this.activeprompt != null) {
              this.activeprompt.dismiss();
              this.activeprompt = prompt;
            }
          }
          else if (data.startsWith('Ringing: ')) {
            data = data.substr('Ringing: '.length);



            let prompt = this.alertCtrl.create({
              title: 'Anruf eingehend (' + data + ')',
              buttons: [
                {
                  text: 'Abbrechen',
                  handler: data => {
                  }
                },
                {
                  text: 'Annehmen',
                  handler: data => {
                    this.wsSend("ANNEHMEN");
                  }
                }
              ]
            });
            prompt.present();
            if (this.activeprompt != null) {
              this.activeprompt.dismiss();
              this.activeprompt = prompt;
            }
          }
          */
        });
      }
      else if (event.category === 'disconnect') {
        this.connected = false;
        setTimeout(f => { this.socket.initialize(); }, 5000);
      }
      else if (event.category === 'connect') {
        this.connected = true;
      }
    });
  }

  calculateStyle_Time(entry) {
    
    var style = {};
    if (entry.heute == true) {
      style['color'] = 'dimgray';
      style['font-weight'] = 'bold';
    }
    else {
      style['color'] = 'dimgray';
    }
    return style;
  }

  calculateStyle_Btn(entry, sender) {
    var style = {
      'font-size': '1.6rem',
      'height': '40px',
      'background-color': '#488aff'
    }

    if (sender == 'call')
    {
      style['background-color'] = '#32db64';
      if (entry.angerufen == true)
      {
        style['background-color'] = '#F5B841';
      }
    }
    return style;
  }



  doCall(entry) {

    entry.angerufen = true;
    this.wsSend('doCall/' + entry.telefonNF);
  }


  doDone(entry) {

    var idx = this.entries.indexOf(entry);
    this.entries.splice(idx, 1);

    this.wsSend('doDone/' + entry.id);
  }

  initCalls() {
    this.wsSend('getCalls');
  }

  wsSend(newMsg) {
    if (newMsg) {
      this.socket.sendMessage(newMsg);
    }
  }

  showPrompt(entry) {
    let prompt = this.alertCtrl.create({
      title: 'Bemerkung',
      // message: "Bemerkung ändern:",
      inputs: [
        {
          name: 'desc',
          placeholder: 'Bemerkung eingeben..',
          value: entry.desc
        },
      ],
      buttons: [
        {
          text: 'Abbrechen',
          handler: data => {
          }
        },
        {
          text: 'Speichern',
          handler: data => {
            entry.desc = data.desc;
          }
        }
      ]
    });
    prompt.present();
  }
}
