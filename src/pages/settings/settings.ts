import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  myserver: string;
  rueckrufe: boolean;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public storage: Storage) {
    this.getSettings();
  }



  setSettings() {
    this.storage.set('server', this.myserver);
    this.storage.set('rueckrufe', this.rueckrufe);

    console.log("setSettings");
  }

  getSettings() {
    this.storage.get('server').then((value) => {
      if (value)
        this.myserver = value;
      else
        this.myserver = '';
    }).catch(() => this.myserver = '');

    this.storage.get('rueckrufe').then((value) => {
      if (value)
        this.rueckrufe = value;
      else
        this.rueckrufe = false;
    }).catch(() => this.rueckrufe = false);

    
  }

}
